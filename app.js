const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const port = 3000;

const { routes } = require('./routes/index');

const app = express();

// view engine setup


async function serverStart() {

  app.use(express.json());



  // Seguridad; ver https://github.com/helmetjs/helmet

  // Incorporar rutas

  routes(app);



  let port = 3000;

  app.listen(port, () => {

   console.log("Servidor ejecutandose en el puerto: " + port);

  });

}



async function cleanup() {

 console.log("Desconectando...");



  process.exit(0);

}



// Captura de señales de S.O. en las que ejecutaremos el cierre de conexiones

process.on("SIGTERM", cleanup);

process.on("SIGINT", cleanup);

process.on("SIGHUP", cleanup);



serverStart();